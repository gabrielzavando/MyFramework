const gulp = require('gulp')
const ts = require('gulp-typescript')
const babel = require('gulp-babel')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')
const pug = require("gulp-pug")
const gulpPugBeautify = require('gulp-pug-beautify')
const htmlmin = require('gulp-htmlmin')
const browserSync = require('browser-sync').create()
const sass = require('gulp-sass')
const autoprefixer = require('gulp-autoprefixer')
let cleanCSS = require('gulp-clean-css')
const imagemin = require('gulp-imagemin')

//Tareas

gulp.task('script', function () {
	// Seleccionar todos los archivos .ts
	return gulp.src('./src/ts/*.ts')
		.pipe(ts({
			noImplicitAny: true
		}))
		.pipe(concat('script.min.js'))
		.pipe(uglify())
	// Destino del proyecto compilado
		.pipe(gulp.dest('./dist/js'))
});

// //1°Toma los archivos js de la carpeta js, los pasa por babel, luego los concatena en un solo archivo, luego minifica ese archivo y, por último, lo envia a la carpeta de distribución js.
//
// gulp.task('script', () => {
// 	gulp.src('./src/js/*.js') //estas rutas están correctas.
// 		.pipe(babel({
// 			presets: ['es2015']
// 		}))
// 		.pipe(concat('script.min.js'))
// 		.pipe(uglify())
// 		.pipe(gulp.dest('./dist/js'))
// })


// //2°Toma cualquier archivo pug, lo pasa a html, lo minifica y crea un archivo html en la raíz si este no existe.

gulp.task('pug2html', function buildHTML() {
	gulp.src('./src/pug/html/*.pug')
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulpPugBeautify({ omit_empty: true }))
		//.pipe(htmlmin({ collapseWhitespace: true })) //Activar para minificar
		.pipe(gulp.dest('./dist/'))
})

// //3°Toma el archivo styles.scss, lo pasa a css, pone los prefijos, avisa si hay un error y crea el archivo styles.css en la carpeta css si este no existe.

gulp.task('sass', function () {
	return gulp.src('./src/scss/styles.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(cleanCSS())
		.pipe(gulp.dest('./dist/css'))
		.pipe(browserSync.stream())
})
//
// //4°Crea un servidor interno, observa y actualiza automaticamente los cambios realizados en los archivos; styles.scss, *.pug, *.js y *.html. Además mantiene las tareas programadas actualizandolas automaticamente.
gulp.task('server', function () {

	browserSync.init({
		server: "./dist/"
	})

	gulp.watch('./src/scss/**/*.scss', ['sass']) // Esto no está cambiando el navegador
	gulp.watch('./src/pug/**/*.pug', ["pug2html"]) //Esto no está recargando el navegador
	gulp.watch('./src/ts/*.ts').on('change', browserSync.reload)
	gulp.watch('./dist/*.html').on('change', browserSync.reload)
})

//5°Optimiza las imagenes de la carpeta img, crea la carpeta img-min y pone las imagenes optimizadas en esa carpeta.

gulp.task('img', () =>
	gulp.src('./src/img/*')
		.pipe(imagemin({ optimizationLevel: 5, progressive: true, interlaced: true }))
		.pipe(gulp.dest('./dist/img-min'))
)

// 6°Con solo escribir Gulp en la terminal activa por defecto todas las tareas entre corchetes.

gulp.task('default', ['script', 'pug2html', 'sass', 'server', 'img'])